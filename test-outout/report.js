$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "Upwork Site Login Failed Feature",
  "description": "",
  "id": "upwork-site-login-failed-feature",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Upwork Site Login Test Failing When Wrong Email Is Provided",
  "description": "",
  "id": "upwork-site-login-failed-feature;upwork-site-login-test-failing-when-wrong-email-is-provided",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "upwork site url is provided",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user opens the url he gets redirected to home page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user clicks the login link",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user provides invalid email address in username or email box",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user click continue with email button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "message displaye Oops! Username is incorrect",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "close the browser",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.upwork_site_url_is_provided()"
});
formatter.result({
  "duration": 8660425200,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.user_opens_the_url_he_gets_redirected_to_home_page()"
});
formatter.result({
  "duration": 32889600,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.user_clicks_the_login_link()"
});
formatter.result({
  "duration": 5047939100,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.user_provides_invalid_email_address_in_username_or_email_box()"
});
formatter.result({
  "duration": 764800200,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.user_click_continue_with_email_button()"
});
formatter.result({
  "duration": 309884600,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.message_displaye_Oops_Username_is_incorrect()"
});
formatter.result({
  "duration": 555942900,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedInvalidEmailStepDefinition.close_the_browser()"
});
formatter.result({
  "duration": 909454400,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Upwork Site Login Test Failing When No Credentials Provided",
  "description": "",
  "id": "upwork-site-login-failed-feature;upwork-site-login-test-failing-when-no-credentials-provided",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 17,
  "name": "upwork site url is given",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user opens the url he gets redirect to home page",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "user clicks the login link on page",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "user click continue with email button present",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "validate fix the errors below",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "validate this field is requred",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "closing the browser",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.upwork_site_url_is_given()"
});
formatter.result({
  "duration": 7478202800,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.user_opens_the_url_he_gets_redirect_to_home_page()"
});
formatter.result({
  "duration": 141527100,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.user_clicks_the_login_link_on_page()"
});
formatter.result({
  "duration": 3795634500,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.user_click_continue_with_email_button_present()"
});
formatter.result({
  "duration": 1815727200,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.validate_fix_the_errors_below()"
});
formatter.result({
  "duration": 61277800,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.validate_this_field_is_requred()"
});
formatter.result({
  "duration": 48001100,
  "status": "passed"
});
formatter.match({
  "location": "LoginFailedNoCredentialsStepDefinition.closing_the_browser()"
});
formatter.result({
  "duration": 827012400,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Verification Of Error Message For Submitting A Proposal Without Any Details",
  "description": "",
  "id": "upwork-site-login-failed-feature;verification-of-error-message-for-submitting-a-proposal-without-any-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 28,
  "name": "open chrome browser",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "user browse url for upwork site he gets redirected to home page",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "user login using email and password",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "user search for QA Automation in search box",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "user clicks on second job to see details",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "user click submit the proposal button and proposal window gets open",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "user scrolls down to the end of the page",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "user clicks on Submit a Proposal without any details",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "verify error message",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "browser closed",
  "keyword": "Then "
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.open_chrome_browser()"
});
formatter.result({
  "duration": 1515154100,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_browse_url_for_upwork_site_he_gets_redirected_to_home_page()"
});
formatter.result({
  "duration": 10496626900,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_login_using_email_and_password()"
});
formatter.result({
  "duration": 3876611400,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_search_for_QA_Automation_in_search_box()"
});
formatter.result({
  "duration": 18777745500,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_clicks_on_second_job_to_see_details()"
});
formatter.result({
  "duration": 1056859500,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_click_submit_the_proposal_button_and_proposal_window_gets_open()"
});
formatter.result({
  "duration": 2139200000,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_scrolls_down_to_the_end_of_the_page()"
});
formatter.result({
  "duration": 5748412400,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.user_clicks_on_Submit_a_Proposal_without_any_details()"
});
formatter.result({
  "duration": 342693700,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.verify_error_message()"
});
formatter.result({
  "duration": 277735700,
  "status": "passed"
});
formatter.match({
  "location": "SubmitProposalWithoutAnyDetailsStepDefinition.browser_closed()"
});
formatter.result({
  "duration": 950346100,
  "status": "passed"
});
});