package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;

public class SubmitProposalWithoutAnyDetailsStepDefinition {
	
	WebDriver driver;
	
	@Given("^open chrome browser$")
	public void open_chrome_browser() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Taimoor\\Downloads\\chromedriver.exe"); 
		driver = new ChromeDriver();
	}

	@When("^user browse url for upwork site he gets redirected to home page$")
	public void user_browse_url_for_upwork_site_he_gets_redirected_to_home_page() {
		driver.get("https://www.upwork.com/");
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String actualTitle = driver.getTitle();
		System.out.println("Title of the page is --> "+actualTitle);
		driver.findElement(By.linkText("Log In")).click();
	}

	@Then("^user login using email and password$")
	public void user_login_using_email_and_password() {
		 String actualTitle = driver.getTitle();
		 System.out.println("Title of the page is --> "+actualTitle);
	     driver.findElement(By.id("login_username")).sendKeys("mr.taimoor.rb@gmail.com");
	     driver.findElement(By.id("login_password_continue")).click();
	     WebElement password = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.id("login_password")));
	     password.sendKeys("!@#Tahira123");
	     driver.findElement(By.id("login_control_continue")).click();
	     
	}

	@Then("^user search for QA Automation in search box$")
	public void user_search_for_QA_Automation_in_search_box() {
	   driver.manage().window().maximize();
	   driver.findElement(By.id("search-box-el")).sendKeys("QA Automation");
	   driver.findElement(By.xpath("//span[@class='glyphicon air-icon-search m-0']")).click();
	}

	@And("^user clicks on second job to see details$")
	public void user_clicks_on_second_job_to_see_details() {
		driver.findElement(By.cssSelector("#layout > div.layout-page-content > div > div.row.filters-container > div > div > div > div > div > div > section:nth-child(2) > div > div > div > div > div.col-md-10 > h4 > a")).click();
		
	}

	@Then("^user click submit the proposal button and proposal window gets open$")
	public void user_click_submit_the_proposal_button_and_proposal_window_gets_open() {
		driver.findElement(By.xpath("//button[starts-with(@id,'submit-proposal-button')]")).click();
	}

	@Then("^user scrolls down to the end of the page$")
	public void user_scrolls_down_to_the_end_of_the_page() {
		Set<String> handler = driver.getWindowHandles();
		java.util.Iterator<String> it = handler.iterator();
		String parentWindowID = it.next(); 
		System.out.println("Parent window id: "+parentWindowID);
		String childWindowID = it.next();
		System.out.println("Child Window ID: "+childWindowID);
		driver.switchTo().window(childWindowID);
		scrollPageDown(driver);
	}

	@And("^user clicks on Submit a Proposal without any details$")
	public void user_clicks_on_Submit_a_Proposal_without_any_details() {
	    driver.findElement(By.xpath("//a[normalize-space()='Submit a Proposal']")).click();
	}

	@Then("^verify error message$")
	public void verify_error_message() {
		String expectedMessage = "Please fix the errors below";
		String actualMessage = driver.findElement(By.xpath("//span[normalize-space()='Please fix the errors below']")).getText();
		Assert.assertEquals("Error message is incorrect", expectedMessage, actualMessage);
	    
	}

	@Then("^browser closed$")
	public void browser_closed() {
	    driver.quit();
	}
	
	public static void scrollPageDown(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

}
