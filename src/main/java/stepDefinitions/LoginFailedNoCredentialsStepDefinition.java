package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginFailedNoCredentialsStepDefinition {
	
	WebDriver driver;
	String notificationAlertText = "Please fix the errors below.";
	String notificationFieldErrorText = "This field is required";

	  @Given("^upwork site url is given$") public void
	  upwork_site_url_is_given() {
			System.setProperty("webdriver.chrome.driver","C:\\Users\\Taimoor\\Downloads\\chromedriver.exe"); 
			driver = new ChromeDriver();
			driver.get("https://www.upwork.com/");
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  
	 }
	  
	  @When("^user opens the url he gets redirect to home page$")
	  public void user_opens_the_url_he_gets_redirect_to_home_page() {
		  String actualTitle = driver.getTitle();
		  System.out.println("Title of the page is --> "+actualTitle);
	  }

	  @Then("^user clicks the login link on page$")
	  public void user_clicks_the_login_link_on_page() {
		  driver.findElement(By.linkText("Log In")).click();
	  }
	  
	  @Then("^user click continue with email button present$")
	  public void user_click_continue_with_email_button_present() {
	      driver.findElement(By.id("login_password_continue")).click();
	  }
	  
	  @Then("^validate fix the errors below$")
	  public void validate_fix_the_errors_below() {
		  String actualNotificationText= driver.findElement(By.xpath("//div[@class='d-none d-md-block']//span[contains(text(),'Please fix the errors below.')]")).getText();
		  Assert.assertEquals( "Alert validation is incorrect", notificationAlertText, actualNotificationText);
	  }
	  
	  @And("^validate this field is requred$")
	  public void validate_this_field_is_requred() {
		  String actualNotificationText= driver.findElement(By.xpath("//span[normalize-space()='This field is required']")).getText();
		  Assert.assertEquals( "Field error notification is incorrect", notificationFieldErrorText, actualNotificationText);
	  }
	  
	  @Then("^closing the browser$")
	  public void closing_the_browser() {
		  driver.quit();
	  }

}
