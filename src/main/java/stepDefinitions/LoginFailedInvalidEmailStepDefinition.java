package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class LoginFailedInvalidEmailStepDefinition {
	
	WebDriver driver;
	String expectedNotificationText = "Oops! Username is incorrect.";

	  @Given("^upwork site url is provided$") 
	  public void upwork_site_url_is_provided() {
			/*
			 * System.setProperty("webdriver.gecko.driver",
			 * "C:\\Users\\Taimoor\\Downloads\\geckodriver.exe"); driver = new
			 * FirefoxDriver();
			 */
		  System.setProperty("webdriver.chrome.driver", "C:\\Users\\Taimoor\\Downloads\\chromedriver.exe");
		  driver = new ChromeDriver();
		  driver.get("https://www.upwork.com/");
		  //driver.manage().window().maximize();
		  driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  
	 }
	  
	  @When("^user opens the url he gets redirected to home page$")
	  public void user_opens_the_url_he_gets_redirected_to_home_page() {
		  String actualTitle = driver.getTitle();
		  System.out.println("Title of the page is --> "+actualTitle);
	  }

	  @Then("^user clicks the login link$")
	  public void user_clicks_the_login_link() {
		  driver.findElement(By.linkText("Log In")).click();
	  }

	  @Then("^user provides invalid email address in username or email box$")
	  public void user_provides_invalid_email_address_in_username_or_email_box() {
		  String actualTitle = driver.getTitle();
		  System.out.println("Title of the page is --> "+actualTitle);
	      driver.findElement(By.id("login_username")).sendKeys("taimoor.nadeem@outlook.com");
	  }

	  @And("^user click continue with email button$")
	  public void user_click_continue_with_email_button() {
	      driver.findElement(By.id("login_password_continue")).click();
	  }

	  @Then("^message displaye Oops! Username is incorrect$")
	  public void message_displaye_Oops_Username_is_incorrect() {
		  String actualNotificationText= driver.findElement(By.xpath("//span[normalize-space()='Oops! Username is incorrect.']")).getText();
		  Assert.assertEquals( "Notification coming is not correct", expectedNotificationText, actualNotificationText);
	  }
	  
	  @Then("^close the browser$")
	  public void close_the_browser() {
		  driver.quit();
	  }
}
