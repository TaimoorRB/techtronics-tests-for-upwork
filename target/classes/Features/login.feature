Feature: Upwork Site Login Failed Feature

Scenario: Upwork Site Login Test Failing When Wrong Email Is Provided

Given upwork site url is provided
When user opens the url he gets redirected to home page
Then user clicks the login link
Then user provides invalid email address in username or email box
And user click continue with email button
Then message displaye Oops! Username is incorrect
Then close the browser



Scenario: Upwork Site Login Test Failing When No Credentials Provided

Given upwork site url is given
When user opens the url he gets redirect to home page
Then user clicks the login link on page
Then user click continue with email button present
Then validate fix the errors below
And validate this field is requred
Then closing the browser



Scenario: Verification Of Error Message For Submitting A Proposal Without Any Details

Given open chrome browser
When user browse url for upwork site he gets redirected to home page
Then user login using email and password
Then user search for QA Automation in search box
And user clicks on second job to see details
Then user click submit the proposal button and proposal window gets open
Then user scrolls down to the end of the page
And user clicks on Submit a Proposal without any details
Then verify error message
Then browser closed
